'use client';

const Success: React.FC = () => {
  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded shadow-md w-full max-w-md text-center">
        <h1 className="text-2xl font-bold mb-6 text-gray-800">Account Created Successfully!</h1>
        <p className="text-gray-700">Your infrastructure is being set up. You will receive an email once it is ready.</p>
      </div>
    </div>
  );
};

export default Success;