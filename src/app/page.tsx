'use client'; // Ajoutez ceci en haut du fichier
import { useRouter } from 'next/navigation';

const Home: React.FC = () => {
  const router = useRouter();

  const handleStart = () => {
    router.push('/name');
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded shadow-md w-full max-w-md text-center">
        <h1 className="text-3xl font-bold mb-6 text-gray-800">Welcome to Morning News SaaS</h1>
        <p className="mb-6 text-gray-600">
          This access is restricted (1000$). <a href="https://morningnews.dev/contact-us" className="text-blue-500 underline">Contact us</a> to unlock the creation of a new SaaS app.
        </p>
        <p className="mb-6 text-gray-600">Create your account to get started.</p>
        <button
          onClick={handleStart}
          className="w-full bg-blue-500 text-white p-2 rounded mt-4"
        >
          Get Started
        </button>
      </div>
    </div>
  );
};

export default Home;