'use client'; // Ajoutez ceci en haut du fichier
import { useState } from 'react';
import { useRouter } from 'next/navigation';
import AnimatedPage from '../../components/AnimatedPage';
import ProgressBar from '../../components/ProgressBar';

const EmailForm: React.FC = () => {
  const [email, setEmail] = useState('');
  const router = useRouter();

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    localStorage.setItem('email', email);
    router.push('/password');
  };

  return (
  <AnimatedPage>
    <div className="min-h-screen flex flex-col items-center justify-center bg-gray-100">
      <ProgressBar step={2} steps={3} />
      <div className="bg-white p-8 rounded shadow-md w-full max-w-md mt-4">
        <h1 className="text-2xl font-bold mb-6 text-gray-800">Enter Your Email</h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label className="block text-gray-700">Email:</label>
            <input
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
              className="w-full p-2 border border-gray-300 rounded mt-1 text-gray-800 bg-white"
            />
          </div>
          <button type="submit" className="w-full bg-blue-500 text-white p-2 rounded mt-4">Next</button>
        </form>
      </div>
    </div>
  </AnimatedPage>
  );
};

export default EmailForm;