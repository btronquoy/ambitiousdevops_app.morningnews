'use client';
import { useState } from 'react';
import { useRouter } from 'next/navigation';
import axios from 'axios';
import AnimatedPage from '../../components/AnimatedPage';
import ProgressBar from '../../components/ProgressBar';

const PasswordForm: React.FC = () => {
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const specialString = process.env.NEXT_PUBLIC_SPECIAL_STRING; // Access environment variable

    if (password !== specialString) {
      alert('Password must match the special string provided!');
      return;
    }

    const user = {
      firstName: localStorage.getItem('firstName'),
      lastName: localStorage.getItem('lastName'),
      email: localStorage.getItem('email'),
    };

    setLoading(true);

    try {
      const response = await axios.post('/api/create-user', user);
      if (response.status === 200) {
        router.push('/success');
      }
    } catch (error) {
      console.error('Error creating user:', error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <AnimatedPage>
      <div className="min-h-screen flex flex-col items-center justify-center bg-gray-100">
        <ProgressBar step={3} steps={3} />
        <div className="bg-white p-8 rounded shadow-md w-full max-w-md">
          <h1 className="text-2xl font-bold mb-6 text-gray-800">Enter Your Password</h1>
          <form onSubmit={handleSubmit}>
            <div className="mb-4">
              <label className="block text-gray-700">Password:</label>
              <input
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
                className="w-full p-2 border border-gray-300 rounded mt-1 text-gray-800 bg-white"
              />
            </div>
            <button type="submit" className="w-full bg-blue-500 text-white p-2 rounded mt-4">
              {loading ? 'Loading...' : 'Submit'}
            </button>
            {loading && <p className="mt-4 text-blue-500">Your account is beeing created. Please wait a little :)</p>}
          </form>
        </div>
      </div>
    </AnimatedPage>
  );
};

export default PasswordForm;
