'use client';
import { useState } from 'react';
import { useRouter } from 'next/navigation';
import AnimatedPage from '../../components/AnimatedPage';
import ProgressBar from '../../components/ProgressBar';

const NameForm: React.FC = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const router = useRouter();

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    localStorage.setItem('firstName', firstName);
    localStorage.setItem('lastName', lastName);
    router.push('/email');
  };

  return (
    <AnimatedPage>
      <div className="min-h-screen flex flex-col items-center justify-center bg-gray-100">
        <ProgressBar step={1} steps={3} />
        <div className="bg-white p-8 rounded shadow-md w-full max-w-md mt-4">
          <h1 className="text-2xl font-bold mb-6 text-gray-800">Enter Your Name</h1>
          <form onSubmit={handleSubmit}>
            <div className="mb-4">
              <label className="block text-gray-700">First Name:</label>
              <input
                type="text"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                required
                className="w-full p-2 border border-gray-300 rounded mt-1 text-gray-800 bg-white"
              />
            </div>
            <div className="mb-4">
              <label className="block text-gray-700">Last Name:</label>
              <input
                type="text"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
                className="w-full p-2 border border-gray-300 rounded mt-1 text-gray-800 bg-white"
              />
            </div>
            <button type="submit" className="w-full bg-blue-500 text-white p-2 rounded mt-4">Next</button>
          </form>
        </div>
      </div>
    </AnimatedPage>
  );
};

export default NameForm;