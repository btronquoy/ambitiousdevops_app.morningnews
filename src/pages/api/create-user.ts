import { NextApiRequest, NextApiResponse } from 'next';
import { MongoClient } from 'mongodb';
import AWS from 'aws-sdk';

const uri = process.env.MONGODB_URI;
if (!uri) {
  throw new Error('Please add your Mongo URI to .env.local');
}
const client = new MongoClient(uri);

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});

const lambda = new AWS.Lambda();

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method !== 'POST') {
    return res.status(405).json({ message: 'Method not allowed' });
  }

  const { firstName, lastName, email } = req.body;

  if (!firstName || !lastName || !email) {
    return res.status(400).json({ message: 'Missing required fields' });
  }

  try {
    await client.connect();
    const db = client.db();
    const result = await db.collection('users').insertOne({ firstName, lastName, email });

    const params = {
      FunctionName: 'sendDiscordMessage',
      InvocationType: 'Event',
      Payload: JSON.stringify({ email }),
    };

    lambda.invoke(params, (err, data) => {
      if (err) {
        console.error('Error invoking Lambda function:', err);
        return res.status(500).json({ error: 'Error creating infrastructure' });
      } else {
        console.log('Lambda function invoked:', data);
        return res.status(200).json({ message: 'User created and infrastructure setup triggered' });
      }
    });
  } catch (error) {
    console.error('Error connecting to MongoDB:', error);
    return res.status(500).json({ error: 'Error creating user' });
  } finally {
    await client.close();
  }
}
